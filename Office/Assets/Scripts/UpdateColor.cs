﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateColor : MonoBehaviour
{
    Button button;
    //public Material mat;
    public Color col;
    public string keyName;
    public TextureMenu main;


    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => {

            SendUpdate();

        });
    }
    private void Update()
    {
/*
        if (Input.GetButtonDown(keyName))
        {
            SendUpdate();
        }*/

    }
    private void SendUpdate()
    {
        main.UpdateColorMaterial(col);
    }
}
