﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlacementHandler : MonoBehaviour
{
    public Transform pos;
   public GameObject obj;
    Button button;

    private void Start()
    {
        pos = transform.Find("PlaceHolderForMenu").transform;
        button = GetComponent<Button>();
        button.onClick.AddListener(UpdatePos);
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            UpdatePos();
        }
    }
    public void UpdatePos()
    {
        obj.transform.position = pos.position;
        obj.transform.rotation = pos.rotation;
        obj.SetActive(true);
    }
}
