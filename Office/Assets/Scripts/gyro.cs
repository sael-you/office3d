using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gyro : MonoBehaviour
{


    private bool gyroEnabled;
    private Gyroscope gyros;
    public GameObject player;

    private GameObject cameraContainer;
    private Quaternion rot;
 #if UNITY_ANDROID || UNITY_IOS
    private void Start()
    {
        cameraContainer = new GameObject("Camera Container");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);
        cameraContainer.transform.SetParent(player.transform);
        cameraContainer.AddComponent<CapsuleCollider>();
        cameraContainer.GetComponent<CapsuleCollider>().radius = 0.34f;
        cameraContainer.GetComponent<CapsuleCollider>().height = 1.94f;
        cameraContainer.tag = "Player";
        cameraContainer.layer = 9;
        gyroEnabled = EnableGyro();
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyros = Input.gyro;
            gyros.enabled = true;

            cameraContainer.transform.rotation = Quaternion.Euler(90f, 90f, 0f);
            cameraContainer.transform.position = player.transform.localPosition;
            rot = new Quaternion(0, 0, 1, 0);

            return true;
        }
        return false;
    }
    private void Update()
    {
        if (gyroEnabled)
        {
            transform.localRotation = gyros.attitude * rot;
        }
    }
#endif
}
