﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCharacterControl : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 1f;
    public Transform cam;
    public VariableJoystick variableJoystick;
    public GameObject joystick;

    
    public float Speed;

    void Update ()
    {
#if UNITY_EDITOR
        PlayerMovement();
#endif
#if PLATFORM_ANDROID
        JoyPlayerMovement();
#endif
    }
    #if PLATFORM_ANDROID
    // Update is called once per frame
    void JoyPlayerMovement()
    {
        joystick.SetActive(true);
        float horizontal = variableJoystick.Horizontal;
        float vertical = variableJoystick.Vertical;
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targetAngel = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg + cam.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0f, targetAngel, 0f);

            Vector3 moveDir = Quaternion.Euler(0f, targetAngel, 0f) * Vector3.forward;
            controller.Move(moveDir * speed * Time.deltaTime);
        }
    }
#endif
#if UNITY_EDITOR
    void PlayerMovement()
    {
        joystick.SetActive(false);
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver).normalized * Speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }
#endif
}