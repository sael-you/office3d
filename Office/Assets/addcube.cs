using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.Interaction.Toolkit.AR;

public class addcube : MonoBehaviour
{
    // Reference to the Prefab. Drag a Prefab into this field in the Inspector.
    public GameObject myPrefab;

    // This script will simply instantiate the Prefab when the game starts.
   public void addobj()
    {
        // Instantiate at position (0, 0, 0) and zero rotation.
        gameObject.SetActive(true);
        Instantiate(myPrefab, new Vector3(this.transform.position.x + 0.3f, this.transform.position.y, 0), Quaternion.identity);

    }
}
