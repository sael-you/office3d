﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public List<Material> lMat;
    public List<GameObject> lObjs;
    public static TextureMenu main;

    private Material[] mats;
    private Material[] activeMats;
    private void Awake() {
        main = this;
    }
    private void Start(){
        
        mats = Resources.LoadAll<Material>("Materials");
       
        
    }

    public void GetMaterialsForObject()
    {
        for(int i = 0; i < lObjs.Count; i++)
        {
            for(int j = 0; j < mats.Length; j++)
            {
                if (mats[j].name.Contains(lObjs[i].name))
                {
                    lMat.Add(mats[j]);
                }
            }
        }
        activeMats = new Material[lObjs.Count];
        for (int i = 0; i < lObjs.Count; i++)
        {
            Material mat = lObjs[i].GetComponent<Renderer>().material;
            activeMats[i] = new Material(mat);
            lObjs[i].GetComponent<Renderer>().material = activeMats[i];
        }
    }

    public void UpdateColorMaterial(Color color)
    {
        for (int i = 0; i < activeMats.Length; i++)
        {
            activeMats[i].color = color;
        }
    }
    public void UpdateMaterial(int index)
    {
        int lenth = lObjs.Count;
        bool foundSomthing = false;
        Material[] activeMat = new Material[lObjs.Count];
        for ( int i = 0; i < lenth; i++)
        {
            
            for (int j = 0; j < lMat.Count; j++)
            {
                if (lMat[j].name.Contains(lObjs[i].name + "0" + index))
                {
                    foundSomthing = true;
                    activeMat[i] = new Material(lMat[j]);
                    lObjs[i].GetComponent<Renderer>().material = activeMat[i];
                    break;
                }
            }
        }
        if (foundSomthing) activeMats = activeMat;
    }
    private void OnDestroy()
    {
        
    }
}
