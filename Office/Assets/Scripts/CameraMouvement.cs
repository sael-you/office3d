﻿using UnityEngine;
using System.Collections;

public class CameraMouvement : MonoBehaviour
{

    /*
    Writen by Windexglow 11-13-10.  Use it, edit it, steal it I don't care.  
    Converted to C# 27-02-13 - no credit wanted.
    Simple flycam I made, since I couldn't find any others made public.  
    Made simple to use (drag and drop, done) for regular keyboard layout  
    wasd : basic movement
    shift : Makes camera accelerate
    space : Moves camera on X and Z axis only.  So camera doesn't gain any height*/


    public float mainSpeed = 10.0f; //regular speed
    public float shiftAdd = 25.0f; //multiplied by how long shift is held.  Basically running
    public float maxShift = 100.0f; //Maximum speed when holdin gshift
    public float camSens = 0.25f; //How sensitive it with mouse
    private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
    private float totalRun = 1.0f;
    public Joystick joystick;
    public GameObject Joystick;
    public GameObject keyboard;

    private void Start()
    {

#if UNITY_ANDROID || UNITY_IOS
        keyboard.SetActive(false);
#endif

    }

    void Update()
    {

#if UNITY_EDITOR || UNITY_WEBGL
        Joystick.SetActive(false);
        this.GetComponent<gyro>().enabled = false;

        lastMouse = Input.mousePosition - lastMouse;
        lastMouse = new Vector3(-lastMouse.y * camSens, Input.GetAxis("Mouse X") * (camSens+5.9f), 0);
        lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
        transform.eulerAngles = lastMouse;
        lastMouse = Input.mousePosition;

        //Mouse  camera angle done.  
#endif
        ////Keyboard commands
        //float f = 0.0f;
        Vector3 p = GetBaseInput();
        //if (Input.GetKey(KeyCode.LeftShift))
        //{
        //    totalRun += Time.deltaTime;
        //    p = p * totalRun * shiftAdd;
        //    p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
        //    p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
        //    p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
        //}
        //else
        //{
        totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);
            p = p * mainSpeed;
        //}

        p = p * Time.deltaTime;
        Vector3 newPosition = transform.position;
        //if (Input.GetKey(KeyCode.Space))
        //{ //If player wants to move on X and Z axis only
            transform.Translate(p);
            newPosition.x = transform.position.x;
            newPosition.z = transform.position.z;
            transform.position = newPosition;
        //}
        //else
        //{
        //    transform.Translate(p);
        //}
    }




    private Vector3 GetBaseInput()
    { //returns the basic values, if it's 0 than it's not active.
        Vector3 p_Velocity = new Vector3();
#if UNITY_EDITOR || UNITY_WEBGL
        //if (Input.GetKey(KeyCode.W))
        //{
        //    p_Velocity += new Vector3(0, 0, 1);
        //}
        //if (Input.GetKey(KeyCode.S))
        //{
        //    p_Velocity += new Vector3(0, 0, -1);
        //}
        //if (Input.GetKey(KeyCode.A))
        //{
        //    p_Velocity += new Vector3(-1, 0, 0);
        //}
        //if (Input.GetKey(KeyCode.D))
        //{
        //    p_Velocity += new Vector3(1, 0, 0);
        //}
        p_Velocity = new Vector3(Input.GetAxis("Horizontal") * 1f, p_Velocity.y, Input.GetAxis("Vertical") * 1f);
#endif

#if UNITY_ANDROID || UNITY_IOS
        p_Velocity = new Vector3(joystick.Horizontal * 1f, p_Velocity.y, joystick.Vertical * 1f);
#endif
        return p_Velocity;
    }
}
