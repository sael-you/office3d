﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class RayCastFromCamera : MonoBehaviour
{
    public Camera cam;
    public EventSystem evedata;
    public GameObject canva;
    public float maxRayDistance = 1000f;
    public GraphicRaycaster ray;
    public TextureMenu textureMenu;



    // Update is called once per frame
    void Update()
    {
        int i = 0;
        
        PointerEventData ped = new PointerEventData(evedata);
        ped.position = Input.mousePosition;

        ped.position = Input.mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        RaycastResult nearhit = new RaycastResult();

        float distance = maxRayDistance;
        Vector3 pointhit = Vector3.zero;

        evedata.RaycastAll(ped, raycastResults);

        foreach (RaycastResult hit in raycastResults)
        {
            if (nearhit.distance < distance)
            {
                if (hit.gameObject.GetComponent<UnityEngine.UI.Button>())
                {
                    nearhit = hit;
                    pointhit = nearhit.worldPosition;
                    i = 2;
                }
                
                distance = hit.distance;
                
            }
                
        }
        RaycastHit[] hits;
        RaycastHit closeHit = new RaycastHit();

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        // Debug.DrawRay(ray.origin, ray.direction, Color.red, 1);
        //ray.origin = -transform.up;

        hits = Physics.RaycastAll(ray);

        foreach(var hit in hits)
        {
            if (hit.distance < distance && hit.transform.tag == "wall")
            {
                closeHit = hit;
                pointhit = hit.point;
                distance = hit.distance;
                i = 1;
            }
        }
       
        if (i == 2)
        {
            nearhit.gameObject?.GetComponent<UnityEngine.UI.Button>().OnPointerEnter(ped);
            nearhit.gameObject?.GetComponent<UnityEngine.UI.Button>().OnPointerExit(ped);

        }

       var inp = Input.GetMouseButtonDown(0);

        if (inp)
        {
            if (i == 2)
            {
                nearhit.gameObject?.GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
            }
            else if (i == 1)
            {
                // GameObject menu = GameObject.Instantiate(canva, transform.position + transform.forward * 3, Quaternion.identity); ;
                
                // activate canvas menu / and set its oriantation towards you .
                canva.SetActive(true);
                if (distance > 1)
                {
                    canva.transform.position = transform.position + transform.forward;
                    canva.transform.LookAt(canva.transform.position + transform.rotation * Vector3.forward, Vector3.up);
                }
                else 
                {
                    canva.transform.position = pointhit + closeHit.normal * 0.05f;
                    canva.transform.LookAt(pointhit + -closeHit.normal, Vector3.up);
                }
                
                
                textureMenu.lObjs.Clear();
                textureMenu.lMat.Clear();

                // get objects
                Transform parentHit = closeHit.transform.parent;
                //Debug.Log(parentHit.name);
                //Debug.Log(closeHit.transform.name);

                //foreach (Transform child in parentHit)
                //{
                //    textureMenu.lObjs.Add(child.gameObject);
                //}
                // change there materials + set active materials for color +
                textureMenu.lObjs.Add(closeHit.transform.gameObject);
                textureMenu.GetMaterialsForObject();

            }
        }
        if (distance == maxRayDistance && inp)
        {
            canva.SetActive(false);
        }
    }
}
